﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForSecondsAndSpin : MonoBehaviour
{
    public float delay;
    public AudioClip startSpinSFX;
    SlotsController _slotsController;

    void Start()
    {
        _slotsController = GetComponent<SlotsController>();
    }

    void OnEnable()
    {
        StartCoroutine(StartSpin(delay));
    }

    IEnumerator StartSpin (float delay)
    {
        yield return new WaitForSeconds(delay);
        _slotsController.Spin(startSpinSFX);
    }
}
