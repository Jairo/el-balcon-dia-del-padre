﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LocalInfoController : MonoBehaviour
{
    public string filename;

    void OnEnable ()
    {
        Text status = GetComponent<Text>();
        if (File.Exists(Path.Combine(Application.persistentDataPath, filename)))
        {
            AppModelInterface[] users = JsonUtility.FromJson<AppModelList>("{\"List\":[" + File.ReadAllText(Path.Combine(Application.persistentDataPath, filename)) + "]}").List;

            status.text = "Personas Registradas " + users.Length;
        }
        else
        {
            status.text = "Personas Registradas 0";
        }
    }
}
