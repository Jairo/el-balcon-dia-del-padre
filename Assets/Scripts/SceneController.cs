﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class SceneController : MonoBehaviour
{
    public float switchDuration;
    Image fader;

    void OnEnable()
    {
        fader = GetComponent<Image>();
        fader.color = Color.white;
        fader.DOColor(new Color(1, 1, 1, 0), switchDuration);
    }

    public void ChangeScene(int sceneIndex)
    {
        fader.gameObject.SetActive(true);
        fader.DOColor(new Color(1, 1, 1, 1), switchDuration);
        StartCoroutine(WaitAndChangeScene(sceneIndex));
    }

    IEnumerator WaitAndChangeScene(int sceneIndex)
    {
        yield return new WaitForSeconds(switchDuration);
        SceneManager.LoadSceneAsync(sceneIndex);
    }
}
