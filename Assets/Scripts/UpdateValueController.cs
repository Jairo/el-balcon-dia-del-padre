﻿using UnityEngine;
using UnityEngine.UI;

public class UpdateValueController : MonoBehaviour
{
    public string sliderId;
    public Text status;

    Text _text;
    Slider _slider;

    void Awake()
    {
        _text = GetComponent<Text>();
        _slider = transform.parent.GetComponent<Slider>();
    }

    void OnEnable()
    {
        _slider.value = PlayerPrefs.GetFloat(sliderId);
    }

    void LateUpdate ()
    {
        _text.text = (_slider.value * 100).ToString("N0") + "%";
    }

    public void Save ()
    {
        PlayerPrefs.SetFloat(sliderId, _slider.value);
    }
}
