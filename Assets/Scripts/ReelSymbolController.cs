﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ReelSymbolController : ExposableMonobehaviour
{
    public ReelSymbolController otherSymbol;
    public bool isSpinning;
    public AudioSource SFXAudioSource;

    public static string[] symbols = { "bigote", "cuoeres", "lentes", "mono", "papa", "sombrero", "tie" };

    [HideInInspector]
    public AudioClip stopSFX;

    [HideInInspector]
    public int stopOnIndex;

    float slotSpeed = 700f;
    int currentSymbolIndex;
    Image _image;
    RectTransform _rectTransform;
    bool timeout;

    // Use this for initialization
    void Start ()
    {
        stopOnIndex = -1;
        currentSymbolIndex = -2;
        isSpinning = false;
        timeout = false;
        _image = GetComponent<Image>();
        _rectTransform = GetComponent<RectTransform>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isSpinning)
        {
            if (currentSymbolIndex == stopOnIndex && _rectTransform.localPosition.y < 20f)
            {
                otherSymbol.GetComponent<Image>().enabled = false;
                _rectTransform.DOLocalMoveY(90f, .24f);
                isSpinning = false;
                timeout = false;
                otherSymbol.isSpinning = false;
                if (stopSFX != null) SFXAudioSource.PlayOneShot(stopSFX);

                StopCoroutine(StartTimeout());
            }

            _rectTransform.localPosition = new Vector3(_rectTransform.localPosition.x, _rectTransform.localPosition.y - slotSpeed * Time.deltaTime);
            if (currentSymbolIndex != stopOnIndex && _rectTransform.localPosition.y < -70f)
            {
                currentSymbolIndex = (timeout) ? stopOnIndex : Random.Range(0, symbols.Length - 1);
                _image.sprite = Resources.Load<Sprite>("Symbols/" + symbols[currentSymbolIndex]);
                _rectTransform.localPosition = new Vector3(_rectTransform.localPosition.x, 300f);
            }
        }
    }

    IEnumerator StartTimeout()
    {
        yield return new WaitForSeconds(10f);
        timeout = true;
    }

    public void Spin ()
    {
        if (!isSpinning)
        {
            isSpinning = true;
            StartCoroutine(StartTimeout());
        }
    }
}
