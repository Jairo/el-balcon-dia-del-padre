﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
static class AppModel
{
    public static string firstName;
    public static string lastName;
    public static long dni;
    public static string birthdate;
    public static string email;
    public static long cellphone;
    public static int winningSymbol;
}

[Serializable]
class AppModelList
{
    public AppModelInterface[] List;
}

[Serializable]
class AppModelInterface
{
    public string firstName;
    public string lastName;
    public long dni;
    public string birthdate;
    public string email;
    public long cellphone;
    public int winningSymbol;
    public int timestamp;

    public AppModelInterface(string _firstName, string _lastName, long _dni, string _birthdate, string _email, long _cellphone, int _winningSymbol)
    {
        firstName = _firstName;
        lastName = _lastName;
        dni = _dni;
        birthdate = _birthdate;
        email = _email;
        cellphone = _cellphone;
        winningSymbol = _winningSymbol;
        timestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
    }
}

public class AppModelController : MonoBehaviour
{
    public string filename;
    public Transform Datos;
    public Transform Instrucciones;
    public Dropdown[] fecha;

    string GetTextFromInputField (GameObject target)
    {
        return target.GetComponent<InputField>().text;
    }

    long GetLongFromInputField (GameObject target)
    {
        try
        {
            return Int64.Parse(target.GetComponent<InputField>().text);
        }
        catch
        {
            return -1;
        }
    }

    public GameObject firstName { set { AppModel.firstName = GetTextFromInputField(value); } }
    public GameObject lastName { set{ AppModel.lastName = GetTextFromInputField(value); } }
    public GameObject dni { set{ AppModel.dni = GetLongFromInputField(value); } }
    public GameObject birthdate { set{ AppModel.birthdate = GetTextFromInputField(value); } }
    public GameObject email { set{ AppModel.email = GetTextFromInputField(value); } }
    public GameObject cellphone { set{ AppModel.cellphone = GetLongFromInputField(value); } }

    public void SetBirthdate ()
    {
        DateTime birthDate;
        string dateString = "";

        dateString = (fecha[1].value + 1).ToString() + "/" + (fecha[0].value + 1).ToString() + "/" + fecha[2].captionText.text;

        if (DateTime.TryParse(dateString, out birthDate))
        {
            AppModel.birthdate = birthDate.ToString("yyyy-MM-dd");
        }
        else
        {
            Debug.Log("RegisterManager::RegisterNoDNI - Error parsing date");
        }
    }

    public void SetWinningSymbol ()
    {
        AppModel.winningSymbol = UnityEngine.Random.Range(0, ReelSymbolController.symbols.Length - 1);
    }

    public void Save (GameObject chkAcceptTermsAndConditions)
    {
        if (chkAcceptTermsAndConditions.GetComponent<UICheckController>().isOn)
        {
            if (!string.IsNullOrEmpty(AppModel.firstName) && !string.IsNullOrEmpty(AppModel.lastName) && !string.IsNullOrEmpty(AppModel.birthdate) && !string.IsNullOrEmpty(AppModel.email) && AppModel.dni != -1 && AppModel.cellphone != -1)
            {
                File.AppendAllText(Path.Combine(Application.persistentDataPath, filename),
                    ((File.Exists(Path.Combine(Application.persistentDataPath, filename))) ? ",\n" : "") +
                    JsonUtility.ToJson(new AppModelInterface(AppModel.firstName, AppModel.lastName, AppModel.dni, AppModel.birthdate, AppModel.email, AppModel.cellphone, AppModel.winningSymbol), true));
            }

            Datos.gameObject.SetActive(false);
            Instrucciones.gameObject.SetActive(true);
        }
    }

    void Start()
    {
        Debug.Log(Application.persistentDataPath);
    }
}
