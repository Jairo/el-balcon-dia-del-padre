﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SlotsController : MonoBehaviour
{
    public ReelSymbolController[] symbolControllers;
    public GameObject thisStep;
    public GameObject nextStep;
    public RawImage prizesImage;
    public Texture[] premios;
    public GameObject seguiParticipando;
    public AudioSource SFXAudioSource;
    public AudioClip rodilloSeDetiene;
    public AudioClip acertaste;
    public AudioClip fallaste;

    private bool isSpinning;

    void Start()
    {
        isSpinning = false;
    }

    public void Spin(AudioClip Knock)
    {
        if (isSpinning) return;
        SFXAudioSource.PlayOneShot(Knock);
        SFXAudioSource.GetComponent<WaitForSecondsAndPlay>().PlayDelayed(.24f);

        isSpinning = true;
        foreach (ReelSymbolController symbolController in symbolControllers)
        {
            symbolController.Spin();
        }

        StartCoroutine(Stop());
    }

    void stopAllOnIndex(int index)
    {
        for (int i = 0; i < symbolControllers.Length; ++i)
        {
            symbolControllers[i].stopSFX = acertaste;
            symbolControllers[i].stopOnIndex = index;
        }
    }

    void stopAllOnIndexExcept(int index, int symbolIndex)
    {
        for (int i = 0; i < symbolControllers.Length; ++i)
        {
            if (i != symbolIndex)
            {
                symbolControllers[i].stopSFX = acertaste;
                symbolControllers[i].stopOnIndex = index;
            }
        }
        symbolControllers[symbolIndex].stopSFX = fallaste;
        symbolControllers[symbolIndex].stopOnIndex = (index < ReelSymbolController.symbols.Length - 1) ? index + 1 : 0;
        symbolControllers[symbolIndex].otherSymbol.stopOnIndex = symbolControllers[symbolIndex].stopOnIndex;

        Debug.Log("SlotsController::stopAllOnIndexExcept - index: " + index + ", symbolIndex: " + symbolIndex);
    }

    void stopWithNoPrize()
    {
        int symbolIndex = Random.Range(0, ReelSymbolController.symbols.Length - 1);
        int indexExcept = Random.Range(0, symbolControllers.Length - 1);
        Debug.Log("SlotsController::stopWithNoPrize - indexExcept: " + indexExcept + ", symbolControllers.Length: " + symbolControllers.Length);

        stopAllOnIndexExcept(symbolIndex, indexExcept);
    }

    IEnumerator Stop()
    {
        yield return new WaitForSeconds(3f);
        float[] prizesProbabilities = { PlayerPrefs.GetFloat("premio1"), PlayerPrefs.GetFloat("premio2"), PlayerPrefs.GetFloat("premio3") };
        bool noPrize = false;

        if (prizesProbabilities[0] + prizesProbabilities[1] + prizesProbabilities[2] > 1)
        {
            float randomNumber = Random.value;
            float allProbabilities = prizesProbabilities[0] + prizesProbabilities[1] + prizesProbabilities[2];
            if (randomNumber < 1 / allProbabilities * prizesProbabilities[0])
            {
                stopAllOnIndex(Random.Range(0, ReelSymbolController.symbols.Length - 1));
                prizesImage.texture = premios[0];
            }
            else if (randomNumber < 1 / allProbabilities * (prizesProbabilities[0] + prizesProbabilities[1]))
            {
                stopAllOnIndex(Random.Range(0, ReelSymbolController.symbols.Length - 1));
                prizesImage.texture = premios[1];
            }
            else
            {
                stopAllOnIndex(Random.Range(0, ReelSymbolController.symbols.Length - 1));
                prizesImage.texture = premios[2];
            }
        }
        else
        {
            float randomNumber = Random.value;
            if (randomNumber < prizesProbabilities[0])
            {
                stopAllOnIndex(Random.Range(0, ReelSymbolController.symbols.Length - 1));
                prizesImage.texture = premios[0];
            }
            else if (randomNumber < prizesProbabilities[0] + prizesProbabilities[1])
            {
                stopAllOnIndex(Random.Range(0, ReelSymbolController.symbols.Length - 1));
                prizesImage.texture = premios[1];
            }
            else if (randomNumber < prizesProbabilities[0] + prizesProbabilities[1] + prizesProbabilities[2])
            {
                stopAllOnIndex(Random.Range(0, ReelSymbolController.symbols.Length - 1));
                prizesImage.texture = premios[2];
            }
            else
            {
                stopWithNoPrize();
                noPrize = true;
            }
        }

        for (int i = 0; i < symbolControllers.Length; ++i)
        {
            if (symbolControllers[i].isSpinning)
            {
                i = 0;
                yield return new WaitForSeconds(.24f);
            }
        }
        SFXAudioSource.Stop();
        SFXAudioSource.PlayOneShot(rodilloSeDetiene);
        yield return new WaitForSeconds(2f);
        thisStep.SetActive(false);

        if (noPrize)
        {
            seguiParticipando.SetActive(true);
        }
        else
        {
            nextStep.SetActive(true);
        }
    }
}
