﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICheckController : ExposableMonobehaviour
{
    [HideInInspector, SerializeField] bool _isOn;
    [ExposeProperty] public bool isOn { set { _isOn = value; UpdateUI(); } get { return _isOn; } }

    void UpdateUI()
    {
        gameObject.SetActive(_isOn);
    }

    public void Toggle ()
    {
        isOn = !_isOn;
    }
}
