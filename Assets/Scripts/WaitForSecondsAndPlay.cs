﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForSecondsAndPlay : MonoBehaviour
{
    AudioSource _audioSource;

	// Use this for initialization
	void Start ()
    {
        _audioSource = GetComponent<AudioSource>();
	}

	public void PlayDelayed (float delay)
    {
        StartCoroutine(Play(delay));
	}

    IEnumerator Play (float delay)
    {
        yield return new WaitForSeconds(delay);
        _audioSource.Play();
    }
}
